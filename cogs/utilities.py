from discord.ext import commands

class Utilities(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command()
    @commands.has_permissions(manage_messages=True)
    async def clear(self, ctx, amt=5):
        """Bulk delete messages. Amount is 5 by default."""
        await ctx.channel.purge(limit=amt+1)

def setup(client):
    client.add_cog(Utilities(client))
