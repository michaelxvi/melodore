import random
import discord
from discord.ext import commands

class Fun(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(name='8ball', aliases=['8'])
    async def _8ball(self, ctx, *, question):
        """Receive the mysteries of your future with the magic 8-Ball!"""
        responses = [
            "It is certain.",
            "It is decidedly so.",
            "Without a doubt.",
            "Yes - definitely.",
            "You may rely on it.",
            "As I see it, yes.",
            "Most likely.",
            "Outlook good.",
            "Yes.",
            "Signs point to yes.",
            "Reply hazy, try again.",
            "Ask again later.",
            "Better not tell you now.",
            "Cannot predict now.",
            "Concentrate and ask again.",
            "Don't count on it.",
            "My reply is no.",
            "My sources say no.",
            "Outlook not so good.",
            "Very doubtful."]
        embed = discord.Embed(title='8-Ball', color=0xFFFFFF)
        embed.add_field(name='Question', value=question, inline=False)
        embed.add_field(name='Answer', value=random.choice(responses), inline=False)
        await ctx.send(embed=embed)

def setup(client):
    client.add_cog(Fun(client))