import discord
from discord.ext import commands

class Mods(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command()
    @commands.has_permissions(kick_members=True)
    async def kick(self, ctx, member : discord.Member, *, reason=None):
        """Kick someone from your server."""
        await member.kick(reason=reason)
        embed = discord.Embed(title="User Kicked", color=discord.Colour.orange())
        embed.set_thumbnail(url=member.avatar_url)
        embed.add_field(name='Name', value=member.mention)
        embed.add_field(name='Reason', value=f'{reason}.')
        await ctx.send(embed=embed)

    @commands.command()
    @commands.has_permissions(ban_members=True)
    async def ban(self, ctx, member: discord.Member, *, reason=None):
        """Ban someone from your server."""
        await member.ban(reason=reason)
        embed = discord.Embed(title="User Banned", color=discord.Colour.red())
        embed.set_thumbnail(url=member.avatar_url)
        embed.add_field(name='Name', value=member.mention)
        embed.add_field(name='Reason', value=f'{reason}.')
        await ctx.send(embed=embed)

    @commands.command()
    @commands.has_permissions(ban_members=True)
    async def unban(self, ctx, *, member):
        """Unban someone from your server."""
        banned_users = await ctx.guild.bans()
        member_name, member_discrim = member.split('#')

        for ban_entry in banned_users:
            user = ban_entry.user
            if (user.name, user.discriminator) == (member_name, member_discrim):
                await ctx.guild.unban(user)
                embed = discord.Embed(title='User Unbanned', color=discord.Color.teal())
                embed.set_thumbnail(url=user.avatar_url)
                embed.add_field(name='Name', value=user.mention)
                await ctx.send(embed=embed)
                break
        else:
            embed = discord.Embed(title='Unban — User not found', color=discord.Color.red())
            embed.add_field(name='The user you specified was not found.', value='If you know the user you specified'
                                                                                    ' is banned,\nplease go to Server'
                                                                                    ' Settings → Bans, and\nmanually'
                                                                                    ' remove the ban there.')
            await ctx.send(embed=embed)

def setup(client):
    client.add_cog(Mods(client))
