**Welcome to Melodore!**

Melodore is a minimalist Discord music bot in development by Abstractus, a Discord bot development team by Jayson Jaskeran and Michael Boyer.

Melodore uses Python and Discord.py, a general purpose programming language and a Discord API wrapper module for Python, respectively.

