import os
import json
import discord
from cryptography.fernet import Fernet
from discord.ext import commands

with open('data/token.key', 'rb') as token_file:
    token_key = token_file.read()
    token_file.close()

with open('data/data.json') as jsonf:
    f = Fernet(token_key)
    data = json.load(jsonf)
    enc_token = (data['enc_token']).encode()
    TOKEN = (f.decrypt(enc_token)).decode()
    PREFIX = data['prefix']

client = commands.Bot(command_prefix=PREFIX)

###################################################

@client.event
async def on_ready():
    await client.change_presence(activity=(discord.Activity(name=f'{PREFIX}help', type=discord.ActivityType.listening)))
    print("Melodore is online.")

@client.event
async def on_member_join(member):
    print(f'{member} has joined a server')

@client.event
async def on_member_remove(member):
    print(f'{member} has left a server')

@client.command()
async def load(ctx, extension):
    """Load a plugin."""
    client.load_extension(f'cogs.{extension}')
    embed = discord.Embed(title=f':white_check_mark: Plugins — Loaded `{extension}`', color=discord.Color.green())
    await ctx.send(embed=embed)

@client.command()
async def unload(ctx, extension):
    """Unload a plugin."""
    client.unload_extension(f'cogs.{extension}')
    embed = discord.Embed(title=f':no_entry: Plugins — Unloaded `{extension}`', color=discord.Color.red())
    await ctx.send(embed=embed)
    return

@client.command()
async def reload(ctx, extension):
    """Reload a plugin."""
    client.unload_extension(f'cogs.{extension}')
    client.load_extension(f'cogs.{extension}')
    embed = discord.Embed(title=f':recycle: Plugins — Reloaded `{extension}`', color=discord.Color.blue())
    await ctx.send(embed=embed)

@client.command()
async def ping(ctx):
    """Ping! Pong!"""
    embed = discord.Embed(title='Melodore — Ping')
    embed.add_field(name='Pong!', value=f'That trip took about {round(client.latency * 1000)} ms.')
    await ctx.send(embed=embed)

###################################################

for filename in os.listdir('cogs'):
    if filename.endswith('.py'):
        client.load_extension(f'cogs.{filename[:-3]}')

client.run(TOKEN)

